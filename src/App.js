import React, { useState } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Dashboard from './Dashboard';
import './App.css'; // Import the App.css file

function App() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [favoriteSeason, setFavoriteSeason] = useState('');
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [errors, setErrors] = useState({});

  const validateEmail = (email) => {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
  };

  const validatePassword = (password) => {
    const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    return re.test(password);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const errors = {};
    if (!/^[a-zA-Z]+$/.test(firstName)) {
      errors.firstName = 'Please enter a valid first name';
    }
    if (!/^[a-zA-Z]+$/.test(lastName)) {
      errors.lastName = 'Please enter a valid last name';
    }
    if (!validateEmail(email)) {
      errors.email = 'Please enter a valid email';
    }
    if (!validatePassword(password)) {
      errors.password = 'Please enter a valid password';
    }
    if (favoriteSeason === '') {
      errors.favoriteSeason = 'Please select a favorite season';
    }
    if (Object.keys(errors).length > 0) {
      setErrors(errors);
      setIsSubmitted(false);
    } else {
      setIsSubmitted(true);
    }
  };

  if (isSubmitted) {
    return (
      <div className="profile-container">
        <h1>Profile</h1>
        <p>First Name: {firstName}</p>
        <p>Last Name: {lastName}</p>
        <p>Email: {email}</p>
        <p>Favorite Season: {favoriteSeason}</p>
        <a href="/dashboard">Dashboard</a>
      </div>
    );
  }

  return (
    <BrowserRouter>
      <div>
        <Switch>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          <Route path="/">
            <h1>Registration Form</h1>
            <form onSubmit={handleSubmit}>
              <div>
                <label>First Name:</label>
                <br/>
                <input
                  type="text"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                />
                {errors.firstName && <div>{errors.firstName}</div>}
              </div>
              <div>
                <label>Last Name:</label>
                <br/>
                <input
                  type="text"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                />
                {errors.lastName && <div>{errors.lastName}</div>}
              </div>
              <div>
                <label>Email:</label>
                <br/>
                <input
                  type="text"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {errors.email && <div>{errors.email}</div>}
              </div>
              <div>
                <label>Password:</label>
                <br/>
                <input
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                {errors.password && <div>{errors.password}</div>}
              </div>
              <div>
                <label>Favorite Season:</label>
                <br/>
                <select
                  value={favoriteSeason}
                  onChange={(e) => setFavoriteSeason(e.target.value)}
                >
                  <option value="">--Select--</option>
                  <option value="Spring">Spring</option>
                  <option value="Fall">Fall</option>
                  <option value="Winter">Winter</option>
                </select>
                {errors.favoriteSeason && <div>{errors.favoriteSeason}</div>}
              </div>
              <button type="submit">Submit</button>
            </form>
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
