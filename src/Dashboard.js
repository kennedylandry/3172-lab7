import React from 'react';
import './App.css'; // Import the App.css file

const Dashboard = () => {
  const userData = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@example.com',
    favoriteSeason: 'Winter',
    accountCreationDate: '2022-02-01',
    lastLoginDate: '2022-03-13',
    numberOfLogins: 12,
  };

  return (
    <div className="dashboard-container"> {/* add a class to the container div */}
      <h1>Welcome to your Dashboard, {userData.firstName}!</h1>
      <h2>Account Information:</h2>
      <p>First Name: {userData.firstName}</p>
      <p>Last Name: {userData.lastName}</p>
      <p>Email: {userData.email}</p>
      <p>Favorite Season: {userData.favoriteSeason}</p>
      <h2>Usage Information:</h2>
      <p>Account Creation Date: {userData.accountCreationDate}</p>
      <p>Last Login Date: {userData.lastLoginDate}</p>
      <p>Number of Logins: {userData.numberOfLogins}</p>
    </div>
  );
};

export default Dashboard;